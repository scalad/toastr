# toastr
**toastr** 是一个非阻塞式的javascript消息通知组件。它依赖于jQuery，它的目标是创建一个简单可自定义和可扩展的核心库。

[![Build Status](https://travis-ci.org/CodeSeven/toastr.svg)](https://travis-ci.org/CodeSeven/toastr)
浏览器测试由BrowserStack提供。

## 当前版本
2.1.3

## 例子
- 可以在[http://codeseven.github.io/toastr/demo.html](http://codeseven.github.io/toastr/demo.html)中找到相关的例子
- [使用FontAwesome图标和toaster](http://plnkr.co/edit/6W9URNyyp2ItO4aUWzBB?p=preview)

## [CDNjs](https://cdnjs.com/libraries/toastr.js)
在CDN JS上面的Toastr

#### 调试版本
- [//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css](//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.css)

#### 压缩版本
- [//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js](//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js)
- [//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css](//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css)

## 安装

#### [NuGet Gallery](http://nuget.org/packages/toastr)
```
Install-Package toastr
```

#### [Bower](http://bower.io/search/?q=toastr)
```
bower install toastr
```

#### [npm](https://www.npmjs.com/package/toastr)
```
npm install --save toastr
```

## Wiki和变更日志
[Wiki和变更日志](https://github.com/CodeSeven/toastr/wiki)

## 不足之处

####动画改变
以下的动画选项已经弃用并且被其他选项替代：

 - 把 `options.fadeIn` 替换成 `options.showDuration`
 - 把 `options.onFadeIn` 替换成 `options.onShown`
 - 把 `options.fadeOut` 替换成 `options.hideDuration`
 - 把 `options.onFadeOut` 替换成 `options.onHidden`

## 快速开始

### 3个简单的步骤
对于其他的API的调用，详细看[例子](http://codeseven.github.io/toastr/demo.html).

1. 添加toastr.css `<link href="toastr.css" rel="stylesheet"/>`

2. 添加toastr.js `<script src="toastr.js"></script>`

3. 使用toastr来显示信息的提醒，成功，警告，错误
	```js
	// 显示一个没有title的提示 title
	toastr.info('Are you the 6 fingered man?')
	```

### 其它选项
```js
// 显示一个没有title的警告toastr
toastr.warning('My name is Inigo Montoya. You killed my father, prepare to die!')

// 显示一个有标题的成功toastr
toastr.success('Have fun storming the castle!', 'Miracle Max Says')

// 显示一个有标题的错误toastr
toastr.error('I do not think that word means what you think it means.', 'Inconceivable!')

// 立即移除当前的toastrs，没有使用动画
toastr.remove()

// 使用动画移除当前的toastr
toastr.clear()

// 重写全局的toastr变量
toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {timeOut: 5000})

// 设置toastr宽度
toastr.success('We do have the Kapua suite available.', 'Turtle Bay Resort', {timeOut: 5000}).css("width","500px");
```

### 避免HTML字符
如果你希望在titile和消息中避免HTML字符，可以如下面这样设置

	toastr.options.escapeHtml = true;

### 关闭按钮
添加一个关闭的按钮
```js
toastr.options.closeButton = true;
````

可选重写关闭按钮的HTML标签

```js
toastr.options.closeHtml = '<button><i class="icon-off"></i></button>';
```

你也可以为 `#toast-container .toast-close-button`重写CSS/LESS

当使点击关闭按钮时该关闭的动画可以被重写
```js
toastr.options.closeMethod = 'fadeOut';
toastr.options.closeDuration = 300;
toastr.options.closeEasing = 'swing';
```

### 显示序列
在底部显示最新的toastr(顶部是默认的)
```js
toastr.options.newestOnTop = false;
```

### 回调
```js
//当toastr被显示、隐藏、点击时为toastr定义回调函数 
toastr.options.onShown = function() { console.log('hello'); }
toastr.options.onHidden = function() { console.log('goodbye'); }
toastr.options.onclick = function() { console.log('clicked'); }
toastr.options.onCloseClick = function() { console.log('close button clicked'); }
```
### 动画选项

Toastr会使用默认的动画，所以你不必提供任何的这些设置，然而如果你喜欢的话你可以重写这些选项

####简单容易
你可以重写显示和隐藏toasts的动画，默认是swing，swing和linear被构建进jQuery中了
```js
toastr.options.showEasing = 'swing';
toastr.options.hideEasing = 'linear';
toastr.options.closeEasing = 'linear';
```

使用jQuery Easing 插件 (http://www.gsgd.co.uk/sandbox/jquery/easing/)
```js
toastr.options.showEasing = 'easeOutBounce';
toastr.options.hideEasing = 'easeInBack';
toastr.options.closeEasing = 'easeInBack';
```

####动画方法
你可以选择使用jQuery显示和隐藏方法，默认是fadeIn\fadeOut，fadeIn/fadeOut, slideDown/slideUp, 和 show/hide方法被构建进jQuery中了。
```js
toastr.options.showMethod = 'slideDown';
toastr.options.hideMethod = 'slideUp';
toastr.options.closeMethod = 'slideUp';
```

###放置重复
以防在同一个堆栈中使用同一个toasts，设置preventDuplicates属性值为true，重复匹配之前的toast是基于它们之前的消息内容。
```js
toastr.options.preventDuplicates = true;
```

###超时
控制toastr和用户交互的时间可以通过设置timeouts属性，timeouts可以通过设置为0而不显示
```js
toastr.options.timeOut = 30; // How long the toast will display without user interaction
toastr.options.extendedTimeOut = 60; // How long the toast will display after a user hovers over it
```


###进度条
直观的显示一个toastr多久到期
```js
toastr.options.progressBar = true;
```

### rtl

Flip the toastr to be displayed properly for right-to-left languages.
```js
toastr.options.rtl = true; 
```

## 编译 Toastr
为了编译压缩版本的Toastr css版本的文件你需要安装[node](http://nodejs.org)

```
npm install -g gulp karma-cli
npm install
```

这时候如果你安装了依赖后然后你就可以编译Toastr

- Run the analytics `gulp analyze`
- Run the test `gulp test`
- Run the build `gulp`

## 贡献

For a pull request to be considered it must resolve a bug, or add a feature which is beneficial to a large audience.

Pull requests must pass existing unit tests, CI processes, and add additional tests to indicate successful operation of a new feature, or the resolution of an identified bug.

Requests must be made against the `develop` branch. Pull requests submitted against the `master` branch will not be considered.

All pull requests are subject to approval by the repository owners, who have sole discretion over acceptance or denial.

## 作者
**John Papa**

+ [http://twitter.com/John_Papa](http://twitter.com/John_Papa)

**Tim Ferrell**

+ [http://twitter.com/ferrell_tim](http://twitter.com/ferrell_tim)

**Hans Fjällemark**

+ [http://twitter.com/hfjallemark](http://twitter.com/hfjallemark)

## Credits
受 https://github.com/Srirangan/notifer.js/. 启发

## 版权
Copyright © 2012-2015

## 许可证
toastr 在 MIT - http://www.opensource.org/licenses/mit-license.php 许可证下